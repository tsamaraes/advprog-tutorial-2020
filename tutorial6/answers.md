# Questions #
1. What is Java Module System, and why use it?
2. What's the difference between a modular system and a normal system (not using modules).
3. The flow of the program, and how the module system works in the program.
4. Create a dependency graph of the project with any tools you like (for instance, draw.io), and place the graph image in the markdown file. Explain the graph you made.

# Answers #
1. Java module system is used to collect Java packages and code into a single unit called module. 
Module itself a collection of java programs or software. 
It is used as a technology that allows older code to be refactored into modules in newer version. 
Java Module System is used to resize and restructure JDK into set of modules so that we can use only required module for our project. 
In module systems, the code will be running in the module path.

2. Without using modular system, the code will be running in the classpath. 
The size application also increased without modular system and difficult to move around. 
With modular system, the code will run in the module path and we can use only the required model of our project.

3.The program starts from the com.guild.main.module, specifically from the Main.java class. The Main.java calls IOUtils.java class that handles the input and output of the program. User will be asked to choose from three distinct options:
  1. Display all the guild employees from the guild.json file
  2. Add a new employee
  3. Exit the program
  
  If user chooses 1, a member of the com.guild.service module, the guildEmployeeService.java class will be called and will get all employees saved and print the information of each employee. 
  If user chooses 2, guildEmployeeService.java class will create a new guildEmployee by calling a new instance, and constructed with specified name and position from user, also wage determined by the position. 
  Next, guildEmployeeService will add the new employee to the guild.json file. 
  If user chooses 3, the program will be terminated by calling system.exit.

4. The graph represents the dependencies of each module. 
Require (written as depends in the graph) means the module
system can enforce the presence of the required module
(reliable configuration), can read the required module 
(readability) and can access public classes exported
in packages (accessibility). Require transitive means that
the module system can read the required module.
![graph](images/graph.png)