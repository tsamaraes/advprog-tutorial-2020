package filemanager;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import fileservice.ResponseFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManager<T> {
    private ResponseFactory<T> responseFactory;
    private ObjectMapper mapper;
    private File targetFile;
    private Class<T> tClass;

    public FileManager(String filename, Class<T> tClass) {
        responseFactory = new ResponseFactory<>();
        mapper = new ObjectMapper();
        targetFile = new File(String.format("static/%s",filename));
        this.tClass = tClass;
    }

    public  Response<T> writeToFileAsJson(T target) {
        List<T> fileCurrentDataContents;
        fileCurrentDataContents = getAllDataFromFIle();
        fileCurrentDataContents.add(target);
        return writeJsonData(target, fileCurrentDataContents);
    }

    private Response<T> writeJsonData(T target , List<T> allJson) {
        try {
            mapper.writeValue(targetFile, allJson);
            return responseFactory.createSuccessResponse("Saved", target);
        } catch (IOException e) {
            return responseFactory.createFailedResponse("IO Exception occurs", target);
        }
    }

    public List<T> getAllDataFromFIle() {
        List<T> allJson;
        try {
            CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);
            allJson = mapper.readValue(targetFile,listType);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            allJson = new ArrayList<>();
        }
        return allJson;
    }

}
