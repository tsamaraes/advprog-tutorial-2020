module com.guild.service {
    exports employeeService;
    requires com.guild.filemanagement;
    requires transitive com.guild.model;
}