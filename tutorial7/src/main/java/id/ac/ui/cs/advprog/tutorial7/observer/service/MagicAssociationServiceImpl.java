package id.ac.ui.cs.advprog.tutorial7.observer.service;

import id.ac.ui.cs.advprog.tutorial7.observer.core.*;
import id.ac.ui.cs.advprog.tutorial7.observer.repository.MagicResearchRepository;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Arcanist;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Elementalist;
import id.ac.ui.cs.advprog.tutorial7.observer.core.GrandMagus;
import id.ac.ui.cs.advprog.tutorial7.observer.core.Researcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class MagicAssociationServiceImpl implements MagicAssociationService {
        private final MagicResearchRepository researchRepository;
        private final MagicAssociation magicAssociation;
        private final Researcher arcanist;
        private final Researcher elementalist;
        private final Researcher grandMagus;

        public MagicAssociationServiceImpl(MagicResearchRepository researchRepository) {
                this.researchRepository = researchRepository;
                this.magicAssociation = new MagicAssociation();
                //ToDo: Complete Me      
                this.arcanist = new Arcanist(this.magicAssociation);
                this.elementalist = new Elementalist(this.magicAssociation);
                this.grandMagus = new GrandMagus(this.magicAssociation);
                this.magicAssociation.add(this.arcanist);
                this.magicAssociation.add(this.elementalist);
                this.magicAssociation.add(this.grandMagus);
        }

        public void addResearch(MagicResearch magicResearch) {
                //ToDo: Complete Me
                this.magicAssociation.addResearch(magicResearch);
        }

        public List<Researcher> getResearchers() {
                return this.magicAssociation.getResearchers();
        }
}
