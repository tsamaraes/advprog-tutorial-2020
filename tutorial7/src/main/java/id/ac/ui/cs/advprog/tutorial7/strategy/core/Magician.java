package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Magician {
	
	private String name;
	private AttackAction attackAction;
	private DefenseAction defenseAction;
	private SupportAction supportAction;

	// TO DO: The constructor still needs additional parameter. Add those needed parameters
	public Magician(String name, AttackAction attackAction, DefenseAction defenseAction, SupportAction supportAction){
		this.name = name;
		// TO DO : initialization for other parameters
		this.attackAction = attackAction;
		this.defenseAction = defenseAction;
		this.supportAction = supportAction;
	}

	public Magician() {

	}
	
	// TO DO : Change the implementation so it can get magician's attack action
	// Hint : Finish the first task (add additional constructor parameter) first
	public AttackAction getAttackAction(){
		return attackAction;
	}
	
	// TO DO : Implement changing AttackAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setAttackAction(AttackAction param){
		this.attackAction = param;
	}
	
	// TO DO : Change the implementation so it can get magician's defense action
	// Hint : Finish the first task (add additional constructor parameter) first
	public DefenseAction getDefenseAction(){
		return defenseAction;
	}
	
	// TO DO : Implement changing DefenseAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setDefenseAction(DefenseAction param){
		this.defenseAction = param;
	}
	
	// TO DO : Change the implementation so it can get magician's support action
	// Hint : Finish the first task (add additional constructor parameter) first
	public SupportAction getSupportAction(){
		return supportAction;
	}
	// TO DO : Implement changing SupportAction
	// Hint : Finish the first task (add additional constructor parameter) first 
	public void setSupportAction(SupportAction param){
		this.supportAction = param;
	}
	
	// TO DO : Implement this method
	public String attack(){
		return this.attackAction.attack();
	}
	
	// TO DO : Implement this method
	public String defense(){
		return this.defenseAction.defense();
	}
	
	// TO DO : Implement this method
	public String support(){
		return this.supportAction.support();
	}
	
	public String getName(){
		return name;
	}


}