package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Elementalist extends Researcher {

        public Elementalist(MagicAssociation magicAssociation) {
                this.name = "Elementalist";
                this.magicAssociation = magicAssociation;
                //ToDo: Complete Me
        }

        public void update() {
                //ToDo: Complete Me
                if ((this.magicAssociation.getResearchType().equals("ElementalMagic") || this.magicAssociation.getResearchType().equals("UniversalMagic"))) {
                        this.getResearchList().add(magicAssociation.getResearch());
                }
        }
}
