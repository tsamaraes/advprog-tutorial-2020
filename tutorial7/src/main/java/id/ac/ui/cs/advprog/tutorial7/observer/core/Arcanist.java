package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Arcanist extends Researcher {

        public Arcanist(MagicAssociation magicAssociation) {
                this.name = "Arcanist";
                this.magicAssociation = magicAssociation;
                //ToDo: Complete Me
        }

        public void update() {
                //ToDo: Complete Me
                if ((this.magicAssociation.getResearchType().equals("ArcaneMagic") || this.magicAssociation.getResearchType().equals("UniversalMagic"))) {
                        this.getResearchList().add(magicAssociation.getResearch());
                }

        }

}
