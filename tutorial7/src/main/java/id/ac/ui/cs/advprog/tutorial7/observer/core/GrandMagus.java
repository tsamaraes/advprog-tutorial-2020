package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class GrandMagus extends Researcher {

        public GrandMagus(MagicAssociation magicAssociation) {
                this.name = "GrandMagus";
                //ToDo: Complete Me
                this.magicAssociation = magicAssociation;
        }

        public void update() {
                //ToDo: Complete Me
                if ((this.magicAssociation.getResearchType().equals("ArcaneMagic") || this.magicAssociation.getResearchType().equals("UniversalMagic")) || this.magicAssociation.getResearchType().equals("ElementalMagic")) {
                        this.getResearchList().add(magicAssociation.getResearch());
                }
        }
}
