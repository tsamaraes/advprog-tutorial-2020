package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Enhance implements SupportAction {
	
	private static final String ACTION_NAME = "Enhance";
	
	public String getDescription(){
		return "A spell that strengthen targeted magician's offensive and defensive power";
	}
	
	public String support(){
		return "Activate " + ACTION_NAME + ". Targeted magician's offensive and defensive power are strengthened";
	}
}