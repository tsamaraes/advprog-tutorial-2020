# Tutorial 2

> Our Legacy Magic Got Seriously Unstable so I Stabilized the Magic by Refactoring the Spell

In your free time, you take your time enjoying sightseeing. You enjoy the sky that looks and feels no different than the sky from your world while you breathe the very same air that you breathed in your past life. It's been two weeks since you are transported into this world yet you feels quite familiar with how things work in this world.

You hear the Guild Master calling you at this leisure time of yours, the Guild Master comes to you and shows you another job for you to do. Apparently the guild has a magic used for managing guild business but somehow has very little functionality.

"You see, this little magic of ours can make things work, every time. But since it offers little outside of that, adventurers still have to use our little slow and old bureaucracy for other uses."

"So, why don't you add functionality to this magic for those other uses?", you ask.

"Well, you see, every time we did that, problems arise. Since you have a knack for magic, perhaps you can help", says the Guild Master. 

You begin to see the spell used to form the magic. Suddenly you feel a strange sensation, a sensation of pain from having knowledge penetrating your mind, just like what you feel when you suddenly remember what unit testing is. Somehow it feels more natural, perhaps because you've felt that before.

You begin to see the spell once again. With the knowledge, you can see code smells everywhere.

"Have you tried to refactor the spell before?", you asked the Guild Master

"Re....factor? What is that?"

"Essentially we decouple a big, complex, and unstable spell into smaller ones. Let me show you how it works"

## Code Smells and Refactoring

Before you explain to the Guild Master what refactoring is, you decide to explain what a Code Smell is. 

"Code Smell, huh?", you utter the words as if those words don't come with the knowledge you suddenly have.

"Code Smell, what is that?", asks the Guild Master

"Code Smells are problems in the code, or in this case, the spell. It does not necessarily mean the spell is wrong, but that there is an underlying problem that can cause even bigger problems in the long term. Bad quality spell will cause trouble, and should be fixed when possible."

"So the process of fixing those unstable spells is called refactoring?"

"You got it right"

You look into the code before you demonstrate how refactoring works. But first, you try to explain to the Guild Master the code's structure:

```
observer
	controller
		ObserverController.java
	core
		Adventurer.java
		Guild.java
		Quest.java
	repository
		QuestRepository.java
	service
		GuildService.java
		GuildServiceImpl.java
Tutorial2Application.java
```

Before you try to refactor the spell, you run `localhost:8080/adventurer-list`, try out the system's magic, and find out everything is working fine. When you add a quest and pick a category, the system saves the quest and adds it to the list of quests for each adventurer. The **Delivery** type quest can be done by all adventurers, the **Escort** type quests can only be done by Knights and Mystic adventurers, and **Rumble** type quests can only be done by Knights and Agile adventurers.

You then notice the tests and see they are all running fine. But something feels wrong. The spell is too unstable. You point out several mistakes:

1. The `Adventurer.java` class holds too many responsibilities to be put as a
   single class. An abstract class approach may be necessary.
2. There are two functions in `Guild.java` that do basically the same thing.
3. Some variables in `Quest.java` can be publicly accessed.

You feel like there are more mistakes in the spell, but these seem the most
relevant. You mark some comments in the spell to remind yourself.

## Sacred Art of SonarCloud

When you look at the code, you remember other tools that can help. I can use SonarCloud for helping me reduce code smells in this application, you thought. Then you try to set the git ready for using SonarCloud. 

You start to chanting the spell, opening [https://sonarcloud.io/](https://sonarcloud.io/). When you open it, Guild Master can't hold his confusion anymore. 

" What is that...?"  Guild Master asks.

You try to calmly finding a good response to that question. Of course you can't answer "a power from other world to solve all of your code smells problem. " You have to find a good reason without explaining how it exists at the first place, especially in this world. 

" A Sacred Art..for helping us finding and fixing code smells! " suddenly you found something good enough to answering the question.

Guild Master's eyes sparkle. You get Guild Master's attention now. As you browse the web, you explaining what Sonar capable of.  

" Sonar can help us identify and fix code smells, vulnerabilities, and other problem. It have dashboard that shows all of the problems in the project. "

" Hmm that helps a lot. All this time I had difficult times to knowing what's wrong with Guild's application. "

You continue to set the Sonar while Guild Master observes beside you. You integrate Sonar with your repository, use your GitLab Account to login here. You also can read the detailed  instruction in the website. 

Next step is to set the repository. If you reading the instruction to set up project, you will see you need to input token after choosing your project. You need to give Access Token to Sonar. You can get it from your gitlab account/settings/Access Token. You access your account settings by clicking your account Avatar.  Give the token the name you like and importantly the scope should include API. This is important step to integrate the repo with Sonar. When successfully create the Access Token, copy it's value. Now you can enter the access token value.

![access-token](images/token.png) 

The next step is to set up some variable. This instruction is clear enough, you just need to enter value from Sonar's Instruction into Gitlab repository. Variable can be accessed from settings/CI CD/ Variables.  After this you will continue to set up some files. 

Guild Master sits curiously while you try to set SonarCloud. You change two files, `build.gradle` and `.gitlab-ci.yml` to set Sonar. You can observe the difference from last quest. You will notice the difference quickly. (When you pulling from upstream the files are already changed, if you reading instruction from Sonar official website, you can skip step for set up this 2 files.) 

Now it is done. Your dashboard now should be still empty. For the starting, push what you have now into master branch before starting any changes. Your dashboard should starting to have some analysis. Like how many bugs it has, how many code smells etc. 
![dahsboard](images/dashboard_after_push.png)

 Now we can start to refactor based on this information. 

`Quest: Resolve as many code smells and vulnerabilities the system has using SonarCloud, without changing the functionality, and still passing all the tests required.`

**IMPORTANT:
The files you can change are not limited to the ones that have `Todo` in them. You are allowed to alter any file (including variable references in test files in order to pass the tests) but you are not allowed to add or remove tests.**

**MORE IMPORTANT:  
Whether you feel like you are finished or not, please DEMONSTRATE your work to the Teaching Assistants. The criteria for this tutorial being scored is demonstrating your work to the teaching assistants, finished or not.**

Checklist:

- [ ] Solve as many problems in the dashboard as you can. Make sure no more warnings from Sonar after the refactor. 
