package id.ac.ui.cs.tutorial4.service;

import id.ac.ui.cs.tutorial4.dataclass.PaperResult;
import id.ac.ui.cs.tutorial4.model.Conference;

/**
 * PaperFinder A service in which it will give you every solution regarding
 * searching for a paper.
 */
public interface PaperFinderService {

    /**
     * Will give a list of paper (if found) about the current conference.
     * 
     * @param conference
     * @return
     */
    PaperResult findPaperFromConference(Conference conference);
}
